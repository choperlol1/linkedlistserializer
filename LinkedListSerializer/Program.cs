﻿namespace LinkedListSerializer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ListRandom randomList = new()
            {
                Count = 3,
                Head = new ListNode() { Data = "Hello!" },
                Tail = new ListNode
                {
                    Data = "Qwerty",
                    Next = new ListNode() { Data = "123" }
                }
            };

            randomList.Head.Random = randomList.Tail.Next;
            randomList.Head.Previous = null!;
            randomList.Head.Next = randomList.Tail;
            randomList.Tail.Random = randomList.Tail;
            randomList.Tail.Previous = randomList.Head;
            randomList.Tail.Next.Random = randomList.Head;
            randomList.Tail.Next.Next = null!;
            randomList.Tail.Next.Previous = randomList.Tail;

            using Stream stream = new MemoryStream();
            randomList.Serialize(stream);
            randomList = new();
            randomList.Deserialize(stream);
        }
    }
}