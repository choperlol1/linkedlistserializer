﻿using System.Runtime.Serialization;
using System.Text;

namespace LinkedListSerializer
{
    public class ListRandom
    {
        public ListNode Head;

        public ListNode Tail;

        public int Count;

        public void Serialize(Stream s)
        {

            Dictionary<ListNode, int> nodeIndexDictionary = new();
            ListNode currentNode = this.Head;
            int indexCounter = 1;

            // Storing nodes and their indexes data into dictionary
            while (currentNode != null)
            {
                nodeIndexDictionary.Add(currentNode, indexCounter);
                indexCounter++;
                currentNode = currentNode.Next;
            }

            StringBuilder stringBuilder = new();
            currentNode = this.Head;

            // Serializing ListRandom class into string
            while (currentNode != null)
            {
                stringBuilder.Append($"\0{currentNode.Data}");
                stringBuilder.Append($"\0{(currentNode.Previous != null ? currentNode.Previous.Data : string.Empty)}");
                stringBuilder.Append($"\0{(currentNode.Next != null ? currentNode.Next.Data : string.Empty)}");
                stringBuilder.Append($"\0{nodeIndexDictionary[currentNode.Random]}");
                currentNode = currentNode.Next!;
            }

            using StreamWriter streamWriter = new(s, Encoding.ASCII, -1, true);

            // Checking if this class is default and writing to stream 
            if (stringBuilder.Length > 0)
            {
                string serializedString = stringBuilder.ToString().Remove(0, 1);
                streamWriter.Write(serializedString);
            }
            else
            {
                // Writing "1" if this class is default
                streamWriter.Write($"1");
            }

            streamWriter.Flush();
        }

        public void Deserialize(Stream s)
        {
            // Checking if stream is empty and populating this class's fields with default values
            if (s.Length == 1)
            {
                this.Head = new ListNode();
                this.Tail = new ListNode();
                this.Count = 0;
                return;
            }
            else if (s.Length <= 0)
            {
                throw new SerializationException("Stream is empty!");
            }

            // Setting position of stream to beginning, reading data from it and splitting into string array
            s.Seek(0, SeekOrigin.Begin);

            // Not disposing stream in case it will be used by caller
            using StreamReader streamReader = new(s, Encoding.ASCII, true, -1, true);
            string serializedString = streamReader.ReadToEnd();
            string[] splitSerializedString = serializedString.Split('\0');

            // One node consists of 4 fields
            if (splitSerializedString.Length % 4 != 0)
                throw new SerializationException("Invalid string format!");

            Dictionary<ListNode, int> nodeIndexDictionary = new();

            // Deserializing string array into ListRandom class, taking a data set of 4 strings each time
            for (int i = 0; i < splitSerializedString.Length; i += 4)
            {
                ListNode tempNode = new()
                {
                    // First string is ListNode.Data
                    Data = splitSerializedString[i],

                    // Second string is ListNode.Previous
                    Previous = splitSerializedString[i + 1] != string.Empty ? new ListNode() { Data = splitSerializedString[i + 1] } : null!,

                    // Third string is ListNode.Next
                    Next = splitSerializedString[i + 2] != string.Empty ? new ListNode() { Data = splitSerializedString[i + 2] } : null!
                };

                // Populating this class with deserialized data
                if (i != 0)
                {
                    this.Tail.Next = tempNode;
                    tempNode.Previous = this.Tail;
                }
                else
                {
                    this.Head = tempNode;
                }

                this.Tail = tempNode;
                this.Count++;

                // Storing nodes and their indexes data into dictionary
                nodeIndexDictionary.Add(tempNode, this.Count);
            }

            // Fourth string is ListNode.Random's index in dictionary
            int indexOfRandomData = 3;
            ListNode currentNode = this.Head;

            // Populating this class with ListNode.Random nodes
            while (currentNode != null)
            {
                currentNode.Random = nodeIndexDictionary.FirstOrDefault(kvp => kvp.Value == int.Parse(splitSerializedString[indexOfRandomData])).Key;
                indexOfRandomData += 4;
                currentNode = currentNode.Next;
            }
        }
    }
}
